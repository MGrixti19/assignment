﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawn : MonoBehaviour {

    public GameObject back;
    float x, y;
    Vector3 startingPosition;


	// Use this for initialization
    void Start()
    {

        listScrpts.check = false;
        listScrpts.listOfSprites.AddRange(Resources.LoadAll<GameObject>("prefabs"));
        listScrpts.listOfSprites.AddRange(Resources.LoadAll<GameObject>("prefabs"));
        while (listScrpts.check == false)
        {
            eighteenF();
        } 
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void eighteenF()
    {
        int location = Random.Range(0, listScrpts.listOfSprites.Count);
        GameObject tempObject = listScrpts.listOfSprites[location];

        if (listScrpts.usedSprites.Contains(tempObject) == false)
        {
            listScrpts.usedSprites.Add(tempObject);


            x = transform.position.x;
            y = transform.position.y;



            transform.position = new Vector3(x, y, 0);
            GameObject.Instantiate(tempObject, transform.position, transform.rotation);

            GameObject.Instantiate(back, transform.position, transform.rotation);

            listScrpts.check = true;
        }
    }

}
