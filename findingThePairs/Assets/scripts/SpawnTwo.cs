﻿using UnityEngine;
using System.Collections;

public class SpawnTwo : MonoBehaviour
{

    public GameObject joker;
    float x, y;
    Vector3 startingPosition;

    // Use this for initialization
    void Start()
    {

        listScrpts.check = false;
        listScrpts.listOfSprites.AddRange(Resources.LoadAll<GameObject>("prefabs2"));
        while (listScrpts.check == false)
        {
            eighteenS();
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    void eighteenS()
    {
        foreach (GameObject temp in listScrpts.listOfSprites)
        {
            Debug.Log(temp.name);
        }

        int location = Random.Range(0, listScrpts.listOfSprites.Count);
        GameObject tempObject = listScrpts.listOfSprites[location];

        if (listScrpts.usedSpritesTwo.Contains(tempObject) == false)
        {

            listScrpts.usedSpritesTwo.Add(tempObject);

            x = transform.position.x;
            y = transform.position.y;

            transform.position = new Vector3(x, y, 0);
            GameObject.Instantiate(tempObject, transform.position, transform.rotation);
            GameObject.Instantiate(joker, transform.position, transform.rotation);
        }
    }
}