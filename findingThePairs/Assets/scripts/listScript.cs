﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class listScrpts : MonoBehaviour
{

    public static List<GameObject> listOfSprites = new List<GameObject>();
    public static List<GameObject> usedSprites = new List<GameObject>();
    public static List<GameObject> listOfSpritesTwo = new List<GameObject>();
    public static List<GameObject> usedSpritesTwo = new List<GameObject>();

    public static bool check = false;

    public static void showBack()
    {
        GameObject[] myBack = GameObject.FindGameObjectsWithTag("back");
        foreach (GameObject temp in myBack)
        {
            temp.GetComponent<SpriteRenderer>().enabled = true;
        }
    }
}