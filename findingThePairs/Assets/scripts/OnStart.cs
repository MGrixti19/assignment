﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class OnStart : MonoBehaviour {

	// Use this for initialization
    public Button myButton;

    void Start()
    {
        Button start = myButton.GetComponent<Button>();
        start.onClick.AddListener(taskOnClick);
    }

    void taskOnClick()
    {
        SceneManager.LoadScene("game");
    }
}
